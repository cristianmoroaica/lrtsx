# Laravel React Typescript Boilerplate

## Included:
* Laravel 6.*
* React 16.13.1
* Hot Module Reloading (npm run dev)
* Admin Middleware (See Below)
* [Typescript](https://www.typescriptlang.org/)
* [Webpack 4](https://webpack.js.org/concepts/)
* [React Router 4](https://reacttraining.com/react-router/web/guides/philosophy)
* [Axios](https://github.com/axios/axios)
* [Lodash (lodash-es)](https://lodash.com/docs/4.17.10)
* [PostCSS](https://github.com/postcss/postcss)
* [PurgeCSS](https://github.com/FullHuman/purgecss)
* [TailwindCSS](https://tailwindcss.com/docs/what-is-tailwind/)
* [Tailwind Forms Plugin](https://tailwindcss-custom-forms.netlify.com/)
* [Ant Design](https://ant.design/docs/react/introduce)
* [FontAwesome 5](http://fontawesome.io/icons/)
* [Emotion CSS-in-JS Library](https://emotion.sh/docs/introduction)
* [Webpack Bundle Analyzer](https://github.com/webpack-contrib/webpack-bundle-analyzer)
* [Log Viewer - /log-viewer (Protected by admin middleware)](https://github.com/ARCANEDEV/LogViewer)
* [Laravel Telescope](https://laravel.com/docs/5.8/telescope)

## Documentation

#### Frontend Files
Frontend JS & CSS files are placed in ``` /frontend ```.

#### Hot Module Reloading and Development:
First, allow insecure certs from localhost (for development).
In Chrome, go to this url and Enable Insecure Certs from Localhost:

``` chrome://flags/#allow-insecure-localhost ```

Run HMR:

``` npm run dev ```

#### Production Development:
``` npm run production ```

To use production built files, set Laravel APP_ENV to production.
#### Admin Middleware

In config/auth.php add the emails of 'Admins' to the admins array.
This allows you to easily restrict access to certain routes whereby the user's email is not in the admins array using the admin middleware.
```
Route::get('admin/profile', function () {
    //
})->middleware('admin');
```

The admin middleware file is located at:
```
App\Http\Middleware\Admin
```