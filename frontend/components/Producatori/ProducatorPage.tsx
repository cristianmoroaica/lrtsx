import React, { useContext, ReactNode } from "react";
import { UserContext } from "contexts/UserContext"
import { ProducatorNavbar } from "./ProducatorNavbar";
import { ProducatorContent } from "./ProducatorContent";

interface IProps {
	children: ReactNode;
}

export const ProducatorHome: React.FC<IProps> = (props) => {
    const [user, setUser] = useContext<any>(UserContext)

    // if(!user) return <Redirect to="/"/>

    return (
        <React.Fragment>
            <ProducatorNavbar />
            <ProducatorContent />
        </React.Fragment>
    );
}