import React, { useContext, useEffect } from "react";
import { UserContext } from "contexts/UserContext"
import { css } from "emotion";

interface IProps {
}

export const ProducatorContent: React.FC<IProps> = () => {
    const [user, setUser] = useContext<any>(UserContext)

    return (
        <div className={css('padding: 4rem; display: flex')}>
          <p>Dashboard producator</p>
        </div>
    );
}