import React, { useContext } from "react";
import { UserContext } from "contexts/UserContext"
import { css } from "emotion";

interface IProps {
}

export const ClientContent: React.FC<IProps> = () => {
    const [user, setUser] = useContext<any>(UserContext)

    return (
        <div className={css('padding: 4rem; display: flex')}>
          <p>Pagina Client</p>
        </div>
    );
}