import React, { useContext, ReactNode } from "react";
import { UserContext } from "contexts/UserContext"
import { css } from "emotion";
import { ClientNavbar } from "./ClientNavbar";
import { ClientContent } from "./ClientContent";

interface IProps {
	children: ReactNode;
}

export const ClientHome: React.FC<IProps> = (props) => {
    const [user, setUser] = useContext<any>(UserContext)

    // if(!user) return <Redirect to="/"/>

    return (
        <React.Fragment>
            <ClientNavbar />
            <ClientContent />
        </React.Fragment>
        
    );
}