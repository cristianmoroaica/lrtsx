import React, { useContext, useEffect } from "react";
import { UserContext } from "contexts/UserContext"
import { css } from "emotion";
import { Link } from 'react-router-dom';

interface IProps {
}

export const ClientNavbar: React.FC<IProps> = () => {
    const [user, setUser] = useContext<any>(UserContext)

    return (
        <div className={css('padding: 4rem; display: flex')}>
            <p>
                {user.name}, {user.role}
            </p>
            <button className="bg-gray-300 m-1 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                <img src="../shopping-cart-solid.svg" alt="" className="fill-current w-4 h-4 mr-2"/>
                <span>Cart</span>
            </button>
            <Link to="/" className="bg-gray-300 m-1 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                <img src="../sign-out-alt-solid.svg" alt="" className="fill-current w-4 h-4 mr-2"/>
                <span>Logout</span>
            </Link>
        </div>
    );
}