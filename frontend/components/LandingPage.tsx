import React, { useContext, useState } from "react";
import { UserContext } from "contexts/UserContext"
import { css } from "emotion";
import { useHistory } from "react-router-dom";

const ReachableContext = React.createContext(null);
const UnreachableContext = React.createContext(null);

interface IProps {

}


export const Producator = {
	id: 0,
	name: "Producator 0",
	email: "email@gmail.com",
	role: 'P'
}

export const Client = {
	id: 1,
	name: "Client 1",
	email: "email@gmail.com",
	role: 'C'
}

const config = {
    title: 'Use Hook!',
    content: (
      <div>
        <ReachableContext.Consumer>{name => `Client login: ${name}!`}</ReachableContext.Consumer>
      </div>
    ),
  };

export const LandingPage: React.FC<IProps> = () => {
    let history = useHistory();
    const [user, setUser] = useContext<any>(UserContext)
    
    const onClientLogin = () => {
        history.push('/client');
        setUser(Client)
    }

    const onProducatorLogin = () => {
        history.push('/dashboard');
        setUser(Producator)
    }

    const promptProducatorLogin = () => {
        setUser(Producator)
    }
    

    return (
        <div className={css(`padding: 4rem`)}>
            <h1>Landing page</h1>

            <div className={"bg-blue-600 text-white p-4 mt-4"}>
                <p className="m-1">
                Demo 1
                </p>
                <ReachableContext.Provider value="Light">
                    <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 m-1 rounded inline-flex items-center" onClick={onClientLogin}>
                        <img src="../user-solid.svg" alt="" className="fill-current w-4 h-4 mr-2"/>
                        <span>Login Client</span>
                    </button>
                </ReachableContext.Provider>
                <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 m-1 rounded inline-flex items-center" onClick={onProducatorLogin}>
                    <img src="../sign-in-alt-solid.svg" alt="" className="fill-current w-4 h-4 mr-2"/>
                    <span>Login Producator</span>
                </button>
            </div>

            
        </div>
    );
}