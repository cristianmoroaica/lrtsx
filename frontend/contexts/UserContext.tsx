import React, { createContext, ReactNode, useState } from "react";

export const StoredUser = null

export interface IUser {
	id: string;
	name: string;
	email: string;
}

interface IProps {
	children: ReactNode;
}

export const UserContext = createContext<IUser | null>(null)
const Provider: any = UserContext.Provider

export const UserContextProvider = (props: IProps) => {
	let [user, setUser] = useState(StoredUser)

	return (
		<Provider value={[user, setUser]}>
			{props.children}
		</Provider>
	)
}