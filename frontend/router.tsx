
import * as React from 'react';
import { hot } from 'react-hot-loader';
import { BrowserRouter , Switch, Route } from 'react-router-dom';
import { LandingPage } from 'components/LandingPage'
import { ClientHome } from 'components/Clienti/ClientPage'
import { ProducatorHome } from 'components/Producatori/ProducatorPage';

export const RouterComponent = hot(module)(() => (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route path="/client" component={ClientHome} />
        <Route path="/dashboard" component={ProducatorHome} />
      </Switch>
    </BrowserRouter>
));
